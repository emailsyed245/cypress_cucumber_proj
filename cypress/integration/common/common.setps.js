import {Given} from "cypress-cucumber-preprocessor/steps"
import {homePage} from '../../support/pages/Home.page'
const url= "https://rahulshettyacademy.com/seleniumPractise/#/"

Given('user has navigated to GreenKart site', ()=>{
    cy.log("user has navigated to GreenKart site")
    cy.visit(Cypress.env('url'))
    // cy.log("Getting thi.data.name value :"+this.data.name)
    
    cy.pause()
})

Given('user has navigated to {string} site', (urlText)=>{
    cy.log("user has navigated to GreenKart site")
    cy.visit(urlText)
    // cy.log("Getting thi.data.name value :"+this.data.name)
    //  cy.wait(3000)
})

When('user adds {string} to the cart', (prodName)=>{
    cy.log(`user adds ${prodName} to the cart`)
    homePage.getProductBtn(prodName).click()
    
})

//-----------API Testing Steps -------

When('user intercepts the api call before clicking the {string} button', (btnText)=>{
    cy.log(`user intercepts the api call before clicking the {string} button`)
    cy.intercept({method: 'GET', 
                  url : 'https://rahulshettyacademy.com/Library/GetBook.php?AuthorName=shetty'}
                  ).as("bookRecord")
    cy.contains('button',btnText).click()             
})

// NOTE : For Mocking in the "intercept" method as a 2nd argument (i.e. mock response)
When('user intercepts the MOCK api call before clicking the {string} button', (btnText)=>{
    cy.log(`user intercepts the MOCK api call before clicking the {string} button`)
    cy.intercept({method: 'GET', 
                  url : 'https://rahulshettyacademy.com/Library/GetBook.php?AuthorName=shetty'}
                  ,{
                     statusCode: 200,
                     body: [{
                       "book_name": "RestAssured with Java",
                       "isbn": "RSU",
                       "aisle": "2301"
                     }],
                  }
                  ).as("bookRecord")
    cy.contains('button',btnText).click()             
})

Then('user should see {string} record', (recordCount)=>{
    cy.log(`user should see ${recordCount} record`)
    cy.wait('@bookRecord')
    cy.get('table.table.table-dark tr').should('have.length',Number(recordCount)+1)
})

When('user makes a POST API request to {string}', (reqURL)=>{
    cy.log(`user makes a POST API request to ${reqURL}`)
    cy.request('POST', reqURL,{
        "name": "Harry Potter Part-3",
        "isbn": "bcdsss",
        "aisle": "2e"+Math.floor(Math.random() * 1000),
        "author": "JK Rowling"}
        ).as('postReq')
})

Then('user should see the success msg {string}', (successMsg)=>{
    cy.log(`user should see the success msg ${successMsg}`)
    cy.get('@postReq').then((response)=>{
        cy.log(response.status)
        cy.log(response.body.Msg)
    })
    // cy.get('table.table.table-dark tr').should('have.length',Number(recordCount)+1)
})