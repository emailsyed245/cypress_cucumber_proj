@test @apiTest
Feature: 4-As a customer, I want to check if the API Calls and Mock them
    
   
    @apiTest1 
    Scenario: 41-Check if API call has correct count for Database
      Given user has navigated to "https://rahulshettyacademy.com/angularAppdemo/" site
      When user intercepts the api call before clicking the "Virtual Library" button
      Then user should see "607" record
       
    @apiTest2
    Scenario: 42-Check if MOCK API call has MOCK response and checks the result correctly
      Given user has navigated to "https://rahulshettyacademy.com/angularAppdemo/" site
      When user intercepts the MOCK api call before clicking the "Virtual Library" button
      Then user should see "1" record
   
    @apiTest3
    Scenario: 43-Check if API POST request can be done too
      When user makes a POST API request to "http://216.10.245.166/Library/Addbook.php"
      Then user should see the success msg "successfully added" 
   