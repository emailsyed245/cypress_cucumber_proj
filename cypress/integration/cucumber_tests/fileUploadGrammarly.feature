@test @fileUpload
Feature: 3-As a customer, I want to check if the document to be uploaded
    
   
    @fileUploaded  
    Scenario: 31-Check if the document to be uploaded correctly
      Given user has navigated to "https://www.grammarly.com/plagiarism-checker" site
      When user uploads the file "TestDoc.rtf"
      Then user should see file text "This is the new document to upload in cypress"
       
    
    