import {Given,When,Then} from "cypress-cucumber-preprocessor/steps"

import {cart} from '../../../support/pages/CartDropdown.page'
// const url= "https://rahulshettyacademy.com/seleniumPractise/#/"


When('user uploads the file {string}', (doc)=>{
    cy.log(`user uploads the file ${doc}`)
    const filePath = 'testDocs/TestDoc.rtf'
    cy.get('input[type="file"]').attachFile(filePath)
    // cy.pause()
})

Then('user should see file text {string}', (fileText)=>{
    cy.log(`user should see file text ${fileText}`)
    cy.wait(2000)
    cy.get("textarea[data-qa='textarea']").then(textArea=>{
        cy.log(textArea.text())
        expect(textArea.text().replace("\n","")).to.contains(fileText)
    })
    cy.wait(3000)
})

// Then('user should see {string} items showing in the cart', (itemCount)=>{
//     cy.log(`user should see "${itemCount}" items showing in the cart`)
//     cart.getCartIcon().click()
//     cart.getCartItems().should('have.length',itemCount)
//     // cy.pause()
// })

// Then('user should see following items with prices', (dataTable)=>{
//     cy.log(`user should see following items with prices`)
//     dataTable.hashes().forEach(item => {
//         cy.log("Item Name : "+item.ItemName)
//         cart.getProductName(item.ItemName).should('be.visible')
//         cart.getProductPrice(item.ItemName).should('have.text',item.Price)
//         cart.getProductName(item.ItemName).siblings().should('have.text',item.Price) // siblings()
//     } )
//     // homePage.getItemCount().should('have.text',`${itemCount}`)
//     // cy.pause()
// })