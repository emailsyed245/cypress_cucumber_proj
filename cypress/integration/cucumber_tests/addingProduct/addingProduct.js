import {Given,When,Then} from "cypress-cucumber-preprocessor/steps"
import {homePage} from '../../../support/pages/Home.page'



When('user adds following items to the cart', (dataTable)=>{
    cy.log(`user adds following items to the cart`)
    const data = dataTable.raw()
    cy.log("dataTable.raw() :"+data[1][0]) // 2nd row 1st column : Mushroom
    cy.log("dataTable.rows :"+dataTable.rows()) //
    cy.log("dataTable.hashes :"+dataTable.hashes())
    dataTable.hashes().forEach(item => {
        cy.log('Adding => '+item.ItemName)
        homePage.getProductBtn(item.ItemName).click()
        homePage.getProductPrice(item.ItemName).then(price => {
            cy.log("Price Text :"+price.text())
            expect(price.text()).to.equal(item.Price)
        })
    })
})

Then('user should see {string} items showing on top right side', (itemCount)=>{
    cy.log(`user should see ${itemCount} items showing on top right side`)
    homePage.getItemCount().should('have.text',`${itemCount}`)
    // cy.pause()
})
