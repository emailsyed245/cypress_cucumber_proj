FROM cypress/base:12.14.0
 
RUN mkdir /app
WORKDIR /app
 
COPY . /app
 
RUN npm install 
 
RUN $(npm bin)/cypress verify
 
RUN ["npm", "run", "run_cucumb_tags"]